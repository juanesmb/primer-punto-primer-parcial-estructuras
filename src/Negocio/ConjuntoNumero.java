/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Util.Conjunto;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MADARME
 */
public class ConjuntoNumero {
    
    private Conjunto<Integer> misNumeros;

    public ConjuntoNumero() {
    }
    
    public ConjuntoNumero(String numeros) {
        
       String []datos=numeros.split(",");
       this.misNumeros=new Conjunto(datos.length);
       for(String myNum:datos)
           try {
               this.misNumeros.adicionarElemento(Integer.parseInt(myNum));
           } catch (Exception ex) {
               System.out.println("Ocurrió un error inesperado :(");
           }
           
        
    }
    
    
    public void miSort() throws Exception
    {
        if(misNumeros.getCapacidad() == 0)
            throw new Exception ("El conjunto se encuentra vacío");
            
        Conjunto<Integer> p = getConjuntoPrimos();
        //System.out.println(p.toString());
        Conjunto<Integer> par = getConjuntoParesNoPrimos();
        //System.out.println(par.toString());
        Conjunto<Integer> impar = getConjuntoImparesNoPrimos();
        
        Conjunto<Integer> n = getConjuntoNegativos();
        
        //limpiamos la lista original
        int t = this.misNumeros.getLength();
        this.misNumeros.removeAll();
        this.misNumeros=new Conjunto(t);
        
        //comenzamos a insertar los datos ordenados
        for (int i = 0; i < p.getLength(); i++) 
        {
            this.misNumeros.adicionarElemento(p.get(i));
        }
        for (int i = 0; i < par.getLength(); i++) 
        {
            this.misNumeros.adicionarElemento(par.get(i));
        }
        for (int i = 0; i < impar.getLength(); i++) 
        {
            this.misNumeros.adicionarElemento(impar.get(i));
        }
        for (int i = 0; i < n.getLength(); i++) 
        {
            this.misNumeros.adicionarElemento(n.get(i));
        }
        
        System.out.println(p.toString());
        System.out.println(par.toString());
        System.out.println(impar.toString());
        System.out.println(n.toString());
          
    }
    
    public String toString()
    {
        return this.misNumeros.toString();
    }

    private Conjunto<Integer> getConjuntoPrimos() throws Exception
    {
        int c = 0;
        
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si el número es 0
            if(this.misNumeros.get(j)==0)
                throw new Exception ("número 0 encontrado en el conjunto");
            //si es primo
            if(esPrimo(this.misNumeros.get(j)) && this.misNumeros.get(j)>0)
            {
                c++;
            }
        }
        
        Conjunto <Integer> p = new Conjunto(c);
        //se insertan los números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si es primo
            if(esPrimo(this.misNumeros.get(j)) && this.misNumeros.get(j)>0)
            {
                p.adicionarElemento(this.misNumeros.get(j));
            }
                
        }
        p.ordenarElementosAscendente();
        return p;
    }

    private Conjunto<Integer> getConjuntoParesNoPrimos() throws Exception
    {
        int c = 0;
        
        //determinamos la cantidad de números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si el número es 0
            if(this.misNumeros.get(j)==0)
                throw new Exception ("número 0 encontrado en el conjunto");
            //si no es primo y es par
            if(!esPrimo(this.misNumeros.get(j)) &&  this.misNumeros.get(j)%2==0 && this.misNumeros.get(j)>0)
            {
                c++;
            }
        }
        
        Conjunto <Integer> p = new Conjunto(c);
        //insertamos los números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si no es primo y es par
            if(!esPrimo(this.misNumeros.get(j)) &&  this.misNumeros.get(j)%2==0 && this.misNumeros.get(j)>0)
            {
                p.adicionarElemento(this.misNumeros.get(j));
            }
                
        }
        p.ordenarElementosDescendente();
        return p;
    }

    private Conjunto<Integer> getConjuntoImparesNoPrimos() throws Exception{
        int c = 0;
        
        //determinamos la cantidad de números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si el número es 0
            if(this.misNumeros.get(j)==0)
                throw new Exception ("número 0 encontrado en el conjunto");
            //si no es primo y es impar
            if(!esPrimo(this.misNumeros.get(j)) &&  this.misNumeros.get(j)%2!=0 && this.misNumeros.get(j)>0)
            {
                c++;
            }
        }
        
        Conjunto <Integer> p = new Conjunto(c);
        //insertamos los números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si no es primo y es par
            if(!esPrimo(this.misNumeros.get(j)) &&  this.misNumeros.get(j)%2!=0 && this.misNumeros.get(j)>0)
            {
                p.adicionarElemento(this.misNumeros.get(j));
            }
                
        }
        p.ordenarElementosAscendente();
        return p;
    }

    private Conjunto<Integer> getConjuntoNegativos() throws Exception{
        int c = 0;
        
        //determinamos la cantidad de números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si el número es 0
            if(this.misNumeros.get(j)==0)
                throw new Exception ("número 0 encontrado en el conjunto");
            //si es negativo
            if(this.misNumeros.get(j)<0)
            {
                c++;
            }
        }
        
        Conjunto <Integer> p = new Conjunto(c);
        //insertamos los números
        for (int j = 0; j < this.misNumeros.getLength(); j++) 
        {
            //si es negativo
            if(this.misNumeros.get(j)<0)
            {
                p.adicionarElemento(this.misNumeros.get(j));
            }
                
        }
        return p;
    }
    
    public boolean esPrimo(int n)
    {
        if(n == 1 || n == 2)
            return true;
        if(n<0)
            return false;
        
        //se empieza desde 2 
        for (int i = 2; i <= n/2; i++) 
        {
            if(n%i==0)
            {
                return false;
            }

        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConjuntoNumero other = (ConjuntoNumero) obj;
        
        boolean igual = this.misNumeros.equals(other);
        
        return igual;
    }
    
    
    
}
